FROM node

LABEL maintainer="Bruno Correia <brunogomescorreia@gmail.com>"

WORKDIR /app

COPY ./app.js /app

RUN npm install express

ENTRYPOINT ["node", "app.js"]
